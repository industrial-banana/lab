# Homelab Kubernetes Cluster Provision
Provisions a homelab Kubernetes cluster based on [k3s](https://k3s.io/), deployed by [Ansible](https://www.ansible.com/).

## Prereqs
* Linux deployment host (E.g Ubuntu) with Ansible CLI tools installed.
* Remote hosts with sudo-permitted ssh user.
* `.env` file populated with values. Use `.env.example` for template.

## Assumptions
* Servers are running Debian-style Linux.
* Arm64 or AMD64 architectures.

## Hosts inventory
All hosts are defined in `src/inventory.yml`.

## Commands
### `make ping`
Test connectivity to inventory hosts.

### `make facts`
Gather and display facts about inventory hosts.

### `make lint`
Run Ansible Lint against the playbooks.

### `make host-bootstrap`
Initializes the remote hosts that comprise the cluster.

**For each node:**
1. Creates the ansible user and group
2. Adds the ansible ssh key to authorized keys list

The ansible user is then used for all subsequent playbooks.

### `make host-provision`
Provisions the Ansible deployment machine and remote hosts by installing needed tools and utilities, along with definining the overall host configuration state.

### `make k3s-install`
Installs K3S server and worker agent binaries to the `servers` and `workers` inventory groups.

### `make k3s-bootstrap`
Deploys initial core services - Argo CD and Bitnami Sealed Secrets - to the cluster. This playbook clones the Argo CD service git repo then applies the bootstrap manifests to the cluster. After installing itself, Argo then pulls the  catlog definitions for cluster-wide services, and syncs/installs those applications such as Longhorn, Prometheus, Traefik, etc.
