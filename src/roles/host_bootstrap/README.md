# Bootstrapping a host
After installing a fresh instance of Linux and creating the bootstrap user, this role connects to the host as that user and creates the Ansible deployment account and SSH key.

The SSH key's extensionless filename is passed via the `DEPLOY_SSH_KEYNAME` environment variable, the public key should be placed in `files/ssh`.