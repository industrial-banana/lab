# K3S Database
Creates an external Postgres DB for use by the cluster, rather than servers using embedded ETCD.