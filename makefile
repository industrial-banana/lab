.PHONY: about ping facts lint

# Tips: https://tech.davis-hansson.com/p/make
SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

PLAY_CMD=ansible-playbook

# Load the env settings so they can be read into the Ansible config
-include ./.env
.EXPORT_ALL_VARIABLES: ;

hosts: host-bootstrap host-provision
k3s: k3s-install k3s-services

about:
> @echo "Kubernetes Homelab Cluster Ansible Configuration"

ping: about
> @echo "Ping test as default deploy user"
> ansible cluster -m ping

facts: about
> @echo "Gather facts on cluster"
> ansible cluster -m setup

lint:
> ansible-lint

host-bootstrap: about lint
> @echo "Bootstrapping inventory hosts as $${BOOTSTRAP_USER}"
> $(PLAY_CMD) src/00-playbook-bootstrap.yml -u $${BOOTSTRAP_USER} --private-key=$${BOOTSTRAP_SSH_KEY_FILE} -K --check

host-provision: about lint
> @echo "Provisioning cluster hosts"
> $(PLAY_CMD) src/01-playbook-provision-hosts.yml

k3s-install: about lint
> @echo "Install Kubernetes"
> $(PLAY_CMD) src/02a-playbook-k3s-install.yml $(params)

k3s-bootstrap: about lint
> @echo "Configuring Kubernetes cluster-wide services"
> $(PLAY_CMD) src/02b-playbook-k3s-services.yml $(params)

# For some weird reason, if pip is called as an Ansible task,
# the subsequent task, no matter what it is, fails with a
# 'worker is in dead state' error.
local_py_dependencies:
> @pip3 install -I kubernetes==17.17.0
> @pip3 install -I openshift==0.12.1